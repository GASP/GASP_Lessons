# GASP Lessons Contributors

### Principal Authors
- Richard Crook
- Gareth McCaughan

### Lead Organizer
- Jeff Elkner

### Writers
- Richard Martinez

### Testers
- Arron Birhanu
- Liam Brown
- Silas Riggs