from gasp import *

GRID_SIZE = 30                     # This sets size of everything
MARGIN = GRID_SIZE                 # How much space to leave round edge

BACKGROUND_COLOR = color.BLACK     # Colors we use
WALL_COLOR = (0.6 * 255, 0.9 * 255, 0.9 * 255)

CHOMP_COLOR = color.YELLOW
CHOMP_SIZE = GRID_SIZE * 0.8        # How big to make Chomp
CHOMP_SPEED = 0.25                  # How fast Chomp moves

FOOD_COLOR = color.RED
FOOD_SIZE = GRID_SIZE * 0.15        # How big to make food


class Immovable:
    def is_a_wall(self):
        return False                # Most objects aren't walls so say no

    def eat(self, chomp):          # Default eat method
        pass                       # Do nothing


class Nothing(Immovable):
    pass


class Movable:
    def __init__(self, maze, point, speed):
        self.maze = maze                      # For finding other objects
        self.place = point                    # Our current position
        self.speed = speed                    # Remember speed

    def furthest_move(self, movement):
        (move_x, move_y) = movement                      # How far to move
        (current_x, current_y) = self.place              # Where are we now?
        nearest = self.nearest_grid_point()              # Where's nearest grid point?
        (nearest_x, nearest_y) = nearest
        maze = self.maze

        if move_x > 0:                                   # Are we moving towards a wall on right?
            next_point = (nearest_x+1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x > nearest_x:         # Are we close enough?
                    move_x = nearest_x - current_x       # Stop just before it

        elif move_x < 0:                                 # Are we moving towards a wall on left?
            next_point = (nearest_x-1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x < nearest_x:         # Are we close enough?
                    move_x = nearest_x - current_x       # Stop just before it

        if move_y > 0:                                   # Are we moving towards a wall above us?
            next_point = (nearest_x, nearest_y+1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y > nearest_y:         # Are we close enough?
                    move_y = nearest_y - current_y       # Stop just before it

        elif move_y < 0:                                 # Are we moving towards a wall below us?
            next_point = (nearest_x, nearest_y-1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y < nearest_y:         # Are we close enough?
                    move_y = nearest_y - current_y       # Stop just before it

        if move_x > self.speed:                          # Don't move further than our speed allows
            move_x = self.speed
        elif move_x < -self.speed:
            move_x = -self.speed

        if move_y > self.speed:
            move_y = self.speed
        elif move_y < -self.speed:
            move_y = -self.speed

        return (move_x, move_y)

    def nearest_grid_point(self):
        (current_x, current_y) = self.place
        grid_x = int(current_x + 0.5)        # Find nearest vertical grid line
        grid_y = int(current_y + 0.5)        # Find nearest horizontal grid line
        return (grid_x, grid_y)              # Return where they cross


class Chomp(Movable):
    def __init__(self, maze, point):
        self.direction = 0                   # Start off facing right
        Movable.__init__(self, maze, point,  # Call Movable initializer
                         CHOMP_SPEED)

    def move(self):
        keys = keys_pressed()
        if   'left' in keys: self.move_left()   # Is left arrow pressed?
        elif 'right' in keys: self.move_right() # Is right arrow pressed?
        elif 'up' in keys: self.move_up()       # Is up arrow pressed?
        elif 'down' in keys: self.move_down()   # Is down arrow pressed?

    def move_left(self):
        self.try_move((-1, 0))

    def move_right(self):
        self.try_move((1, 0))

    def move_up(self):
        self.try_move((0, 1))

    def move_down(self):
        self.try_move((0, -1))

    def try_move(self, move):
        (move_x, move_y) = move
        (current_x, current_y) = self.place
        (nearest_x, nearest_y) = (self.nearest_grid_point())

        if self.furthest_move(move) == (0, 0):         # If we can't move, do nothing.
            return

        if move_x != 0 and current_y != nearest_y:     # If we're moving horizontally
            move_x = 0                                 # but aren't on grid line
            move_y = nearest_y - current_y             # move towards grid line

        elif move_y != 0 and current_x != nearest_x:   # If we're moving vertically
            move_y = 0                                 # but aren't on grid line
            move_x = nearest_x - current_x             # Move towards grid line

        move = self.furthest_move((move_x, move_y))    # Don't go too far
        self.move_by(move)

    def draw(self):
        maze = self.maze
        screen_point = maze.to_screen(self.place)
        angle = self.get_angle()                     # Work out half of mouth angle
        endpoints = (self.direction + angle,         # Rotate according to direction
                     self.direction + 360 - angle)
        self.body = Arc(screen_point, CHOMP_SIZE,    # Draw sector
                        endpoints[0], endpoints[1],
                        filled=True, color=CHOMP_COLOR)

    def get_angle(self):
        (x, y) = self.place                                   # Work out distance
        (nearest_x, nearest_y) = (self.nearest_grid_point())  # to nearest grid point
        distance = (abs(x-nearest_x) + abs(y-nearest_y))      # Between -1/2 and 1/2
        return 1 + 90*distance                                # Between 1 and 46

    def move_by(self, move):
        self.update_position(move)
        old_body = self.body          # Get old body for removal
        self.draw()                   # Make new body
        remove_from_screen(old_body)  # Remove old body

        (x, y) = self.place                        # Get distance to
        nearest_point = self.nearest_grid_point()  # nearest grid point
        (nearest_x, nearest_y) = nearest_point
        distance = (abs(x-nearest_x) +             # As before
                    abs(y-nearest_y))

        if distance < self.speed * 3/4:            # Are we close enough to eat?
            object = self.maze.object_at(nearest_point)
            object.eat(self)                         # If so, eat it

    def update_position(self, move):
        (old_x, old_y) = self.place                     # Get old coordinates
        (move_x, move_y) = move                         # Unpack vector
        (new_x, new_y) = (old_x+move_x, old_y+move_y)   # Get new coordinates
        self.place = (new_x, new_y)                     # Update coordinates

        if move_x > 0:                   # If we're moving right ...
            self.direction = 0           # ... turn to face right.
        elif move_y > 0:                 # If we're moving up ...
            self.direction = 90          # ... turn to face up.
        elif move_x < 0:                 # If we're moving left ...
            self.direction = 180         # ... turn to face left.
        elif move_y < 0:                 # If we're moving down ...
            self.direction = 270         # ... turn to face down.


class Maze:
    def __init__(self):
        self.have_window = False        # We haven't made window yet
        self.game_over = False          # Game isn't over yet
        self.set_layout(the_layout)     # Make all objects
        set_speed(40)                   # Set loop rate to 40 loops per second

    def set_layout(self, layout):
        height = len(layout)                   # Length of list
        width = len(layout[0])                 # Length of first string
        self.make_window(width, height)
        self.make_map(width, height)           # Start new map
        self.movables = []
        self.food_count = 0                    # Start with no Food
        
        max_y = height - 1
        for x in range(width):                 # Go through whole layout
            for y in range(height):
                char = layout[max_y - y][x]    # See discussion 1 page ago
                self.make_object((x, y), char) # Create object

        for movable in self.movables:    # Draw all movables
            movable.draw()

    def make_window(self, width, height):
        grid_width = (width-1) * GRID_SIZE     # Work out size of window
        grid_height = (height-1) * GRID_SIZE
        screen_width = 2*MARGIN + grid_width
        screen_height = 2*MARGIN + grid_height
        begin_graphics(screen_width,           # Create window
                       screen_height,
                       "Chomp",
                       BACKGROUND_COLOR)

    def to_screen(self, point):
        (x, y) = point
        x = x*GRID_SIZE + MARGIN     # Work out coordinates of point
        y = y*GRID_SIZE + MARGIN     # on screen
        return (x, y)

    def make_map(self, width, height):
        self.width = width                 # Store size of layout
        self.height = height
        self.map = []                      # Start with empty list
        for y in range(height):
            new_row = []                   # Make new row list
            for x in range(width):
                new_row.append(Nothing())  # Add entry to list
            self.map.append(new_row)       # Put row in map

    def make_object(self, point, character):
        (x, y) = point
        if character == '%':                         # Is it a wall?
            self.map[y][x] = Wall(self, point)
        elif character == 'P':                       # Is it Chomp?
            chomp = Chomp(self, point)
            self.movables.append(chomp)
        elif character == '.':
            self.food_count = self.food_count + 1    # Add 1 to count
            self.map[y][x] = Food(self, point)       # Put new object in map

    def finished(self):
        return self.game_over        # Stop if game is over

    def play(self):
        update_when('next_tick')     # Just pass time at loop rate

    def done(self):
        end_graphics()               # We've finished
        self.map = []
        self.movables = []

    def object_at(self, point):
        (x, y) = point

        if y < 0 or y >= self.height:         # If point is outside maze,
            return Nothing()                  # return nothing.

        if x < 0 or x >= self.width:
            return Nothing()

        return self.map[y][x]

    def play(self):
        for movable in self.movables:     # Move each object
            movable.move()
        update_when('next_tick')          # Pause before next loop

    def remove_food(self, place):
        (x, y) = place
        self.map[y][x] = Nothing()             # Make map entry empty
        self.food_count = self.food_count - 1  # There is 1 less bit of Food
        if self.food_count == 0:               # If there is no food left...
            self.win()                         #... Chomp wins

    def win(self):
        print "You win!"
        self.game_over = True


class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point                          # Store our position
        self.screen_point = maze.to_screen(point)
        self.maze = maze                            # Keep hold of Maze
        self.draw()

    def draw(self):
        #dot_size = GRID_SIZE * 0.2               # You can remove...
        #Circle(self.screen_point, dot_size,      # ...these three lines...
               #color = WALL_COLOR, filled = 1)   # ...if you like.
        (x, y) = self.place
        neighbors = [ (x+1, y), (x-1, y),        # Make list of our neighbors.
                       (x, y+1), (x, y-1) ]
        for neighbor in neighbors:               # Check each neighbor in turn.
            self.check_neighbor(neighbor)

    def is_a_wall(self):
        return True                 # This object is a wall, so say yes

    def check_neighbor(self, neighbor):
        maze = self.maze
        object = maze.object_at(neighbor)            # Get object

        if object.is_a_wall():                       # Is it a wall?
            here = self.screen_point                 # Draw line from here...
            there = maze.to_screen(neighbor)         # ... to there if it is
            Line(here, there, color=WALL_COLOR, thickness=2)


class Food(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()

    def draw(self):
        self.dot = Circle(self.screen_point,
                          FOOD_SIZE,
                          color=FOOD_COLOR,
                          filled=True)

    def eat(self, chomp):
        remove_from_screen(self.dot)           # Remove dot from screen
        self.maze.remove_food(self.place)      # Tell Maze


# The shape of the maze.  Each character
# represents a different type of object
#   % - Wall
#   . - Food
#   o - Capsule
#   G - Ghost
#   P - Chomp
# Other characters are ignored

the_layout = [
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",     # There are 31 '%'s in this line
  "%.....%.................%.....%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.%.....%......%......%.....%.%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%.%%%.%.%%% %%%.%.%%%.%...%",
  "%.%%%.......%GG GG%.......%%%.%",
  "%...%.%%%.%.%%%%%%%.%.%%%.%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%.%.....%......%......%.....%.%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.....%........P........%.....%",
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"]

the_maze = Maze()                 # Make Maze object

while not the_maze.finished():    # Keep playing until we're done
    the_maze.play()

the_maze.done()                   # We're finished
