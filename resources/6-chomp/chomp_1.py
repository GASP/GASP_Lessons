from gasp import *

GRID_SIZE = 30
MARGIN = GRID_SIZE

BACKGROUND_COLOR = color.BLACK
WALL_COLOR = (0.6 * 255, 0.9 * 255, 0.9 * 255)

class Immovable:
    def is_a_wall(self):
        return False


class Nothing(Immovable):
    pass


class Maze:
    def __init__(self):
        self.have_window = False        # We haven't made the window yet.
        self.game_over = False          # The game isn't over yet.
        self.set_layout(the_layout)     # Make all the objects.
        set_speed(40)                   # Set loop rate to 40 loops per second.
        
    def set_layout(self, layout):
        height = len(layout)                   # The length of the list.
        width = len(layout[0])                 # The length of the first string
        self.make_window(width, height)
        self.make_map(width, height)           # Start a new map

        max_y = height - 1
        for x in range(width):                 # Go through the whole layout
            for y in range(height):
                char = layout[max_y - y][x]    # See the discussion 1 page ago.
                self.make_object((x, y), char) # Create the object

    def make_window(self, width, height):
        grid_width = (width-1) * GRID_SIZE    # Work out the size of the window.
        grid_height = (height-1) * GRID_SIZE
        screen_width = 2*MARGIN + grid_width
        screen_height = 2*MARGIN + grid_height
        begin_graphics(screen_width,          # Create the window.
                       screen_height,
                       "Chomp",
                       BACKGROUND_COLOR)
                       
    def to_screen(self, point):
        (x, y) = point
        x = x*GRID_SIZE + MARGIN     # Work out the coordinates of the point.
        y = y*GRID_SIZE + MARGIN     # on the screen.
        return (x, y)
        
    def make_map(self, width, height):
        self.width = width                 # Store the size of the layout.
        self.height = height
        self.map = []                      # Start with an empty list
        for y in range(height):
            new_row = []                   # Make a new row list
            for x in range(width):
                new_row.append(Nothing())  # Add an entry to the list
            self.map.append(new_row)       # Put the row in the map

    def make_object(self, point, character):
        (x, y) = point
        if character == '%':                    # Is it a wall?
            self.map[y][x] = Wall(self, point)

    def finished(self):
        return self.game_over        # Stop if the game is over.

    def play(self):
        update_when('next_tick')     # Just pass the time.

    def done(self):
        end_graphics()               # We've finished
        self.map = []                # Forget all the objects

    def object_at(self, point):
        (x, y) = point

        if y < 0 or y >= self.height:    # If the point is outside the maze,
            return Nothing()             # return nothing

        if x < 0 or x >= self.width:
            return Nothing()

        return self.map[y][x]


class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point                          # Store our position.
        self.screen_point = maze.to_screen(point)
        self.maze = maze                            # Keep hold of the Maze.
        self.draw()

    def draw(self):
        (x, y) = self.place
        # Make a list of our neighbors
        neighbors = [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]

        for neighbor in neighbors:
            self.check_neighbor(neighbor)            # Check each one in turn

    def is_a_wall(self):
        return True

    def check_neighbor(self, neighbor):
        maze = self.maze
        object = maze.object_at(neighbor)            # Get the object

        if object.is_a_wall():                       # Is is a wall?
            here = self.screen_point                 # Draw a line from here...
            there = maze.to_screen(neighbor)         # ... to there if it is
            Line(here, there, color=WALL_COLOR, thickness=2)


# The shape of the maze.  Each character
# represents a different type of object.
#   % - Wall
#   . - Food
#   o - Capsule
#   G - Ghost
#   P - Chomp
# Other characters are ignored.

the_layout = [
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",     # There are 31 '%'s in this line.
  "%.....%.................%.....%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.%.....%......%......%.....%.%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%.%%%.%.%%% %%%.%.%%%.%...%",
  "%.%%%.......%GG GG%.......%%%.%",
  "%...%.%%%.%.%%%%%%%.%.%%%.%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%.%.....%......%......%.....%.%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.....%........P........%.....%",
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"]


the_maze = Maze()

while not the_maze.finished():
    the_maze.play()
    
the_maze.done()   
