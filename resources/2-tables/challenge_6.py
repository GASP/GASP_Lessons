from gasp import *

num = read_number("What is your favorite number? ")

if num == 100:
    print "Ahh, your favorite number is 100! That's mine, too."
else:
    print "Your favorite number is not the same as mine.  My favorite is 100."
