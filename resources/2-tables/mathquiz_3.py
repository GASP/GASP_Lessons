from gasp import *

n1 = random_between(1, 10)
n2 = random_between(1, 10)

s = "What is " + str(n1) +  " times " + str(n2) + "? "
ans = read_number(s)

if ans == n1 * n2:
    print "That's right -- well done."
else:
    print "No, I'm afraid the answer is " + str(n1 * n2) + "."
