from gasp import *

right = 0

for question in 1, 2, 3, 4, 5, 6, 7, 8, 9, 10:
    n1 = random_between(1, 10)
    n2 = random_between(1, 10)

    s = "What is " + str(n1) +  " times " + str(n2) + "? "
    ans = read_number(s)

    if ans == n1 * n2:
        print "That's right -- well done."
        right = right + 1
    else:
        print "No, I'm afraid the answer is " + str(n1 * n2) + "."

print
print "I asked you 10 questions.  You got " + str(right) + " of them right."
print "Well done!"
