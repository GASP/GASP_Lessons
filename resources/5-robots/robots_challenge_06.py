from gasp import *          # As usual

def place_player():
    global player_x, player_y, player_shape

    player_x = random_between(0, 63)
    player_y = random_between(0, 47)
    player_shape = Circle((10*player_x+5, 10*player_y+5), 5, filled=True)


def place_robot():
    global robot_x, robot_y, robot_shape

    robot_x = random_between(0, 63)
    robot_y = random_between(0, 47)
    robot_shape = Box((10*robot_x, 10*robot_y), 10, 10)


def move_player():
    global player_x, player_y, player_shape

    key = update_when('key_pressed')

    if key == '6' and player_x < 63:
        player_x += 1
    elif key == '3':
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1
    elif key == '2' and player_y > 0:
        player_y -= 1
    elif key == '1':
        if player_x > 0:
            player_x -= 1
        if player_y > 0:
            player_y -= 1
    elif key == '4' and player_x > 0:
        player_x -= 1
    elif key == '7':
        if player_x > 0:
            player_x -= 1
        if player_y < 47:
            player_y += 1
    elif key == '8' and player_y < 47:
        player_y += 1
    elif key == '9':
        if player_x < 63:
            player_x += 1
        if player_y < 47:
            player_y += 1

    move_to(player_shape, (10*player_x+5, 10*player_y+5))


begin_graphics()            # So that you can draw things
finished = False

place_robot()
place_player()

while not finished:
    move_player()

end_graphics()              # Finished!
