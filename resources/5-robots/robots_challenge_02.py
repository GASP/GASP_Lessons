from gasp import *          # As usual

def place_player():
    player_x = random_between(0, 63)
    player_y = random_between(0, 47)
    Circle((10*player_x+5, 10*player_y+5), 5, filled=True)


def move_player():
    print "Press any key to make me move..."
    update_when('key_pressed')


begin_graphics()            # So that you can draw things
finished = False

place_player()

while not finished:
    move_player()

end_graphics()              # Finished!
