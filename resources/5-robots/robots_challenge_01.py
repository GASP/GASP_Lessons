from gasp import *          # As usual

def place_player():
    print "Here I am!"


def move_player():
    print "Press any key to make me move..."
    update_when('key_pressed')


begin_graphics()            # So that you can draw things
finished = False

place_player()

while not finished:
    move_player()

end_graphics()              # Finished!
