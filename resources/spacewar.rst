Space War
=========


Introduction
------------

This is a Python games worksheet. When you've finished it, you'll have written
a version of a classic two-player arcade game called Space War.


What you need to know
---------------------

* The basics of Python (from `Sheet 1 <1-intro.html>`__ (*Introducint Python*)
  and `Sheet 2 <2-tables.html>`__ (*Turning the Tables*)
* Functions from `Sheet 3 <3-pretty.html>`__ (*Pretty Pictures*);
  you might want to look at `Sheet F <F-functions.html>`__ (*Functions*) too
* Classes and Objects (from `Sheet O <O-objects.html>`__ (*Objects and Classes*)

You should definitely look at Sheet O before starting this worksheet, even if
you did the Beginners' course, as this sheet assumes you know about classes and
objects, which aren't in that course.


What is Space War?
------------------

I'll begin by explaining how the game works.

Space war is a 2-dimensional game: what's on the screen is flat rather than
being a view of a 3-D world --- so it's like Worms, say, rather than Quake.

The game starts with the two players' space ships on the screen, along with a
planet at the centre of the screen.

In the game, each player guides their ship around the screen, shooting bullets
at the other player's ship. If a player's ship is hit by a bullet, that player
loses the game.

Each player controls their space ship by rotating it left and right and using
the rocket thrusters on the back of the space ship to push it in the direction
it's pointing in. When the player fires, the bullets come out of the front of
the space ship and travel in the direction the ship is pointing in.

Each ship has a limited amount of fuel, some of which is used up every time the
ship's thrusters are used. When there's no more fuel left on the ship, the
thrusters stop working: the ship can rotate but it cannot thrust.

Everything on the screen feels the gravity of the planet at the centre: if a
ship ventures too close to the planet, it will be sucked in and collide with
it. If a player's ship collides with the planet, that player loses the game.

If both ships collide, the game is a draw.


Things on the screen
--------------------

In the outline of the game we saw above, there are 3 kinds of objects which can
be on the screen. They are:

* the planet
* space ships
* bullets fired by a space ship

The Livewires games library provides us with some shapes which we can put on
the screen, and also a way for them move across the screen by themselves, so we
don't have to keep telling them to move, as you might have done if you've
written games in BASIC before). These things are provided as classes, as you
might expect from `Sheet O <O-objects.html>`__ (*Objects and Classes*). We are
going to make sub-classes of the useful shape classes to create the things we
want on the screen.


The Planet class
----------------

Let's start with the planet, as it's the easiest thing because it doesn't move.
Type the following into the editor window (the editor window is the one without
the ``>>>`` characters before each line. In this window, Python doesn't run
what you type as soon as you type it, but lets you type a lot of things to be
run when you tell it).

.. sourcecode:: python
    
    from livewires import games
    from livewires import colour
    import math

First, we tell Python we wanted to access the things in the LiveWires games
module, using the ``import`` statement. We also want to use the ``math``
module, which comes with Python. (Math is the American abbreviation for
mathematics).

.. sourcecode:: python
    
    SCREENWIDTH = 640
    SCREENHEIGHT = 480
    
    CENTRE_X = SCREENWIDTH / 2
    CENTRE_Y = SCREENHEIGHT / 2

Then we set up ``SCREENWIDTH`` and ``SCREENHEIGHT`` to hold the width and the
height of our graphics window. We did this so we can easily change the width or
height if we want to, just by changing the program at that point. If we'd used
the numbers 640 (the width) and 480 (the height) all over the place and we
decided we wanted to change them to make our screen bigger, we'd have to go
through the program looking for all the times we've used those numbers, work
out whether they were being used for the screen size or something else (eg if
we score 640 points for hitting another ship, we don't want to change that when
we change the screen size), and change the ones that relate to the screen
size. So, we define some variables so we can refer to the sizes by
name instead. Programmers who don't do things like this tend to spend a lot of
time trying to work out how to alter simple things!

Another thing which we're doing here is following a set way of naming our
variables. Things which we won't change during the program have names which are
ALL IN CAPITALS. Python *doesn't* force us to do this, but doing it makes it
easier for someone reading our program to know what sort of thing a variable is
without having to puzzle through the program to work it out.

A thing which we won't change is called a **constant**, by the way.

We work out a couple of other constants from the width and height of the
screen: they're the *x* and *y* co-ordinates of the centre of the screen.

.. sourcecode:: python
    
    class Planet (games.Circle):
        RADIUS = 50
    
        def __init__ (self, screen):
            
            self.init_circle (screen = screen, x = CENTRE_X, y = CENTRE_Y,
              radius = Planet.RADIUS, colour = colour.grey)

Now we create the ``Planet`` class. The ``Planet`` class is a sub-class of the
``Circle`` class. The ``Circle`` class is provided by the LiveWires games
module, so we had to prefix its name by ``games.`` to tell Python where to find
it.

The only method we're defining for the moment is the ``__init__`` method,
which, you'll remember from `Sheet O <O-objects.html>`__ (*Objects and
Classes*), is called when we create a new object from the ``Planet`` class. The
only thing that the ``Planet`` needs to know is which screen to be on, which we
tell it. It works out where it ought to be using the ``CENTRE_X`` and
``CENTRE_Y`` constants, and it has a radius defined inside the class. Planets
are made of space rock, which is a sort of grey colour.

If you want to see the planet on the screen, add the following lines to your
program:

.. sourcecode:: python
    
    my_screen = games.Screen(width = SCREENWIDTH, height = SCREENHEIGHT)
    
    stars = games.load_image ("stars.bmp")
    my_screen.set_background (stars)
    
    my_planet = Planet (screen = my_screen)
    my_screen.mainloop ()

This will create a ``Screen`` (that is, a window) for the objects in the game
to be in, and then put a ``Planet`` in the window. The ``mainloop`` method of
the ``Screen`` class tells Python to start drawing the screen (and to start
moving things about on it, as we'll see in a minute).

Before you run the program, you'll need a picture of a starry sky. You can
download the image file, `stars.bmp <figures/stars.bmp>`__, here.  *NOTE:* The
stars picture shows NGC 1818, a young globular cluster. It was taken by Diedre
Hunter using the Hubble Space Telescope. It was Astronomy Picture of the Day on
March 11, 2001.  See `http://antwrp.gsfc.nasa.gov/apod/ap010311.html
<http://antwrp.gsfc.nasa.gov/apod/ap010311.html>`__ for details.  Move this
image file into the same directory as the Python program you're writing.

If you run the program, you should see the planet at the centre of the screen.
You'll need to close the window to stop the program.

Now we create the ``Ship`` class, to which the players' ships will belong.


The Ship class
--------------

Let's think about the player's space ship. We're going to make the ``Ship``
class out of the ``Polygon`` class. A **polygon** is a many-sided shape, like a
triangle or a square.

The ship is a triangle. It turns out that an isosceles triange that's about 20
pixels high (that is, from the base to the point) looks about right on our
screen. Let's make the base 12 pixels across.

The ``Polygon`` class needs a list the co-ordinates of the points which make up
the polygon. We'll give it a list of three points to make the triangle.

When we're writing co-ordinates, we usually write them with the first number
presenting the x co-ordinate and the second representing the y co-ordinate. So
3 pixels along and 2 up would be ``(3, 2)``.


Conclusion
----------

You've now got a workable Space War game. If you've got time, you might like to
think of ways to add to the game, such as adding powerups to enable the player
to pick up extra fuel, adding things like homing missiles or mines (which just
stay in orbit from where they're laid), and so on. If you need to know more
about the games module to do this, have a look at Sheet~W, where you'll find
a list of all the classes and methods in the module.

Have fun.
