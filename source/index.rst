..
    Copyright Gareth McCaughan and Jeffrey Elkner. All rights reserved.
    
    CONDITIONS:

    A "Transparent" form of a document means a machine-readable form,
    represented in a format whose specification is available to the general
    public, whose contents can be viewed and edited directly and
    straightforwardly with generic text editors or (for images composed of
    pixels) generic paint programs or (for drawings) some widely available
    drawing editor, and that is suitable for input to text formatters or for
    automatic translation to a variety of formats suitable for input to text
    formatters. A copy made in an otherwise Transparent file format whose
    markup has been designed to thwart or discourage subsequent modification
    by readers is not Transparent. A form that is not Transparent is
    called "Opaque". 

    Examples of Transparent formats include LaTeX source and plain text.
    Examples of Opaque formats include PDF and Postscript.  Paper copies of
    a document are considered to be Opaque.

    Redistribution and use of this document in Transparent and Opaque
    forms, with or without modification, are permitted provided that the
    following conditions are met: 

    - Redistributions of this document in Transparent form must retain
      the above copyright notice, this list of conditions and the following
      disclaimer. 

    - Redistributions of this document in Opaque form must reproduce the
      above copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided with
      the distribution, and reproduce the above copyright notice in the
      Opaque document itself.

    - Neither the name of Scripture Union, nor LiveWires nor the names of
      its contributors may be used to endorse or promote products derived
      from this document without specific prior written permission. 

    DISCLAIMER:

    THIS DOCUMENT IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS,
    CONTRIBUTORS OR SCRIPTURE UNION BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS DOCUMENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

GASP Beginners Python Course
============================

Written by Students for Students
--------------------------------

Introduction
------------

Welcome to the GASP Beginners Python Course! A learning place so great, it will
take your breath away.

There are a lot of worksheets in our beginners' Python course. The most
important are the six numbered sheets; you should work your way through those.
But they'll be pretty hard going unless you also make use of the lettered
sheets, which give you useful information about particular topics.

This sheet gives a very brief description of all the sheets.

Each sheet has been given a difficulty rating : one to three stars.

\* Basic.
    This stuff is reasonably easy, and you'll need to know it.

\*\* Intermediate.
    This is trickier, but useful.

\*\*\* Advanced.
    If you're feeling smart and/or adventurous, you might try these.


Numbered worksheets
-------------------

Things to try, programs to write.

`1 <1-intro.html>`__ \* `Introducing Python <1-intro.html>`__ You should read
this first. It's a little tour of some of the simpler things you can do with
Python. 

`2 <2-tables.html>`__ \* `Turning the Tables <2-tables.html>`__ Computers are
better at arithmetic than people. By the end of this sheet you'll have written
a program that tests you on your tables. 

`3 <3-pretty.html>`__ \*\* `Pretty Pictures <3-pretty.html>`__ An introduction
to graphics in Python. This sheet also introduces the important idea of
*functions*. 

`4 <4-highlow.html>`__ \* `Higher! Lower! <4-highlow.html>`__ Writing a
guessing game program. Not very hard, but there are plenty of ideas for taking
it further. 

`5 <5-robots.html>`__ \*\* `The Robots are Coming! <5-robots.html>`__ A
graphical game in which you try to outwit a horde of robots.

`6 <6-chomp.html>`_ \*\*\* `Chomp <6-chomp.html>`__ A gasp version of a classic
arcade game.


Lettered worksheets
-------------------

Important (and a few unimportant) topics.

`A <A-lists.html>`__ \* `Lists <A-lists.html>`__ Lists of things are important
in Python. This sheet shows you some things you can do with them.

`C <C-conds.html>`__ \* `Conditions and Conditionals <C-conds.html>`__ How to
tell the computer Do this if so-and-so is true; otherwise, do that. 

`D <D-dicts.html>`__ \*\* `Dictionaries and Tuples <D-dicts.html>`__
Dictionaries and tuples: two kinds of object that aren't covered elsewhere in
the course. 

`E <E-errors.html>`__ \* `Errors <E-errors.html>`__ (also called Exceptions)
Some rude messages you might get from Python, and what they mean. 

`F <F-functions.html>`__ \*\* `Functions <F-functions.html>`__ An important
tool for organizing programs.

`G <G-gasp.html>`__ \*\* `Gasp <G-gasp.html>`__ More details about drawing
movable shapes with Python.

`I <I-inout.html>`__ \* `Input and Output <I-inout.html>`__ How to make Python
tell you things and ask you questions. 

`J <J-jargon.html>`__ \* `Jargon <J-jargon.html>`__ A guide to some technical
terms. A useful sheet to have around.

`L <L-loops.html>`__ \* `Loops <L-loops.html>`__ How to do things over and over
again. Important.

`M <M-modules.html>`__ \*\*\* `Modules <M-modules.html>`__ An advanced topic:
How Python manages to provide a huge number of useful functions without being
too confusing.

`O <O-objects.html>`__ \*\* `Objects and Classes <O-objects.html>`__ Fairly
easy. Important for the Games worksheets. 

`S <S-strings.html>`__ \* `Strings <S-strings.html>`__ What you can do with
bits of text. 

`T <T-time.html>`__ \*\* `Time <T-time.html>`__ How to tell the time, wait for
0.3 of a second, etc.


.. toctree::
    :maxdepth: 1
    :hidden:

    1-intro.rst
    2-tables.rst
    3-pretty.rst
    4-highlow.rst
    5-robots.rst
    6-chomp.rst


.. toctree::
    :maxdepth: 1
    :hidden:

    A-lists.rst
    C-conds.rst
    D-dicts.rst
    E-errors.rst
    F-functions.rst
    G-gasp.rst
    I-inout.rst
    J-jargon.rst
    L-loops.rst
    M-modules.rst
    O-objects.rst
    S-strings.rst
    T-time.rst

.. toctree::
    :maxdepth: 1
    :hidden:

    LiveWiresDocumentationLicense.rst
