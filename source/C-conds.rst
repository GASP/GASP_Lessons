..
    Copyright Gareth McCaughan and Jeffrey Elkner. All rights reserved.
    
    CONDITIONS:

    A "Transparent" form of a document means a machine-readable form,
    represented in a format whose specification is available to the general
    public, whose contents can be viewed and edited directly and
    straightforwardly with generic text editors or (for images composed of
    pixels) generic paint programs or (for drawings) some widely available
    drawing editor, and that is suitable for input to text formatters or for
    automatic translation to a variety of formats suitable for input to text
    formatters. A copy made in an otherwise Transparent file format whose
    markup has been designed to thwart or discourage subsequent modification
    by readers is not Transparent. A form that is not Transparent is
    called "Opaque". 

    Examples of Transparent formats include LaTeX source and plain text.
    Examples of Opaque formats include PDF and Postscript.  Paper copies of
    a document are considered to be Opaque.

    Redistribution and use of this document in Transparent and Opaque
    forms, with or without modification, are permitted provided that the
    following conditions are met: 

    - Redistributions of this document in Transparent form must retain
      the above copyright notice, this list of conditions and the following
      disclaimer. 

    - Redistributions of this document in Opaque form must reproduce the
      above copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided with
      the distribution, and reproduce the above copyright notice in the
      Opaque document itself.

    - Neither the name of Scripture Union, nor LiveWires nor the names of
      its contributors may be used to endorse or promote products derived
      from this document without specific prior written permission. 

    DISCLAIMER:

    THIS DOCUMENT IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS,
    CONTRIBUTORS OR SCRIPTURE UNION BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS DOCUMENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Conditions and Conditionals
===========================


Introduction
------------

**Conditions** are things that can be True or False. For instance, the
expression ``x < 3`` which is True if ``x`` is the name of a number less than 3
and False otherwise.

**Conditionals** are things that depend on conditions. The most important kind
of conditional in Python is the ``if`` statement, which lets you do one thing
or another depending on whether a condition is True.

This sheet tells you about conditions and conditionals.


Conditions
----------

Try typing these things in. We haven't shown the answers, because you'll learn
better if you try them.

.. sourcecode:: python
    
    >>> 1 < 2   # 1 is less then 2, so this condition is True
    [CENSORED]
    >>> 1 > 2   # 1 is not greater than 2, so this condition is False
    [CENSORED]

``True`` and ``False`` (be sure to capitalize the first letter) represent the
truth value of conditions. They are special Python values used for things that
are either True or False.

You can actually use other things as truth values. We don't recommend this,
though; it's just likely to be confusing.


Comparisons
-----------

Most conditions are comparisons of one object with another. Here's a brief list
of ways to compare things in Python.

``a < b``
    True if ``a`` is less than ``b``

``a <= b``
    True if ``a`` is less than or equal to ``b``

``a > b``
    True if ``a`` is greater than ``b``

``a >= b``
    True if ``a`` is greater than or equal to ``b``

``a == b``
    True if ``a`` is equal to ``b``
    
``a != b``
    True if ``a`` is not equal to ``b``

It's pretty obvious what these things mean when ``a`` and ``b`` are numbers.
But they make sense for other sorts of objects, too. For instance, strings are
compared in something rather like alphabetical order. In fact, you can compare
*any* two objects, though many of the possible comparisons are *very silly*.
For instance, it turns out that Python thinks that 3 is less than ``'silly'``.

Be careful, by the way, to notice the difference between ``=`` and ``==``.  You
use ``=`` for setting a variable (i.e., giving a name to an object), and ``==``
for testing whether two things are equal.


Combining comparisons
~~~~~~~~~~~~~~~~~~~~~

You can say things like ``1 < x < 2``, meaning 1 is less than ``x``, and ``x``
is less than 2 .


Other conditions
----------------

Here are some other useful conditions.

``0``
    Always False

any non-zero number
    Always True

``x in y``
    True if ``x`` is equal to some element of ``y``

``x not in y``
    True if ``x`` is not equal to any element of ``y``

an empty list, tuple, or dictionary ( ``[]``, ``()``, or ``{}``)
    Always False

a list, tuple, or dictionary that is not empty
    Always True

(*note*: For ``in`` and ``not in``, ``y`` should be a sequence: that is, a list
or a tuple or a string.)


Combining conditions
--------------------

You can join conditions together using the words ``and``, ``or`` and ``not``
So, for instance, ``x < 3 and y > 6`` is a condition.


The ``if`` statement
--------------------

So, now we know about conditions. One very important thing we can do with them
is to use them in an ``if`` statement. This is pretty straightforward:

.. sourcecode:: python
    
    if x < 3:
        print("x is less than 3. I'm setting it to 3.")
        x = 3

Often, you want to do one thing if a condition is True and another thing if the
condition is False. To do this, use the magic word ``else``:

.. sourcecode:: python
    
    if x < 3:
        print("x is less than 3.")
    else:
        print("x is not less than 3.")

And, less often, you want to test a whole bunch of conditions and do something
according to the first one that comes out True. For this, you need the strange
word ``elif``, which is short for ``else if``:

.. sourcecode:: python
    
    if x < 3:
        print("x is less than 3")
    elif x < 4:
        print("x is not less than 3, but it's less than 4.")
    else:
        print("x isn't even less than 4.")


Other uses of conditions
------------------------

Conditions are also used in ``while`` loops: to learn about those, see
`Sheet L <L-loops.html>`__ (*Loops*).
