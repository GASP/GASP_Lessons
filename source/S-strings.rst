..
    Copyright Gareth McCaughan and Jeffrey Elkner. All rights reserved.
    
    CONDITIONS:

    A "Transparent" form of a document means a machine-readable form,
    represented in a format whose specification is available to the general
    public, whose contents can be viewed and edited directly and
    straightforwardly with generic text editors or (for images composed of
    pixels) generic paint programs or (for drawings) some widely available
    drawing editor, and that is suitable for input to text formatters or for
    automatic translation to a variety of formats suitable for input to text
    formatters. A copy made in an otherwise Transparent file format whose
    markup has been designed to thwart or discourage subsequent modification
    by readers is not Transparent. A form that is not Transparent is
    called "Opaque". 

    Examples of Transparent formats include LaTeX source and plain text.
    Examples of Opaque formats include PDF and Postscript.  Paper copies of
    a document are considered to be Opaque.

    Redistribution and use of this document in Transparent and Opaque
    forms, with or without modification, are permitted provided that the
    following conditions are met: 

    - Redistributions of this document in Transparent form must retain
      the above copyright notice, this list of conditions and the following
      disclaimer. 

    - Redistributions of this document in Opaque form must reproduce the
      above copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided with
      the distribution, and reproduce the above copyright notice in the
      Opaque document itself.

    - Neither the name of Scripture Union, nor LiveWires nor the names of
      its contributors may be used to endorse or promote products derived
      from this document without specific prior written permission. 

    DISCLAIMER:

    THIS DOCUMENT IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS,
    CONTRIBUTORS OR SCRIPTURE UNION BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS DOCUMENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Strings
=======


Introduction
------------

A string is a piece of text. Strings are made up of characters; a character is
a single letter, digit, symbol or whatever. Python provides lots of things for
doing with strings; this sheet tells you about some of them.


Entering strings
----------------

You can put a string into your program by putting it inside quotation marks.
Single quotes and double quotes are both fine, but you must use the same sort
of quotation mark at the start and at the end!

In these worksheets, I've usually used single quotes. If you prefer double
quotes, that's fine. If you want a string that contains a single-quote
character (which also serves as the apostrophe), you should surround it with
double quotes: ``"I'm bored"`` rather than ``'I'm bored'`` (which won't work at
all: can you see why?)


Strings as sequences
--------------------

There are some things that work on lists as well as on strings. They work
because both lists and strings are sequences . Here's a very brief summary.
You might want to compare with with `Sheet A <A-lists.html>`__ (*Lists*).

.. sourcecode:: python
    
    >>> thing = 'I am the walrus'      # A string to work with
    >>> gloop = "Another string"       # And another one
    >>> thing + gloop                  # String concatenation...
    'I am the walrusAnother string'    # quad works as you might guess
    >>> 2 * thing                      # String replication...
    'I am the walrusI am the walrus'   # quad also does what you'd think
    >>> thing[0]                       # We start counting at 0
    'I'                                # quad so we get the first character
    >>> thing[1:5]                     # Characters 1 (inclusive) to 5 (exclusive)
    ' am '                             # quad so 4 characters in all
    >>> len(thing)                     # How many characters?
    15                                 # That many!


String methods
--------------

Strings have a whole lot of useful methods (see `Sheet O <O-objects.html>`_)
that you can use.  Here are a few useful ones:

.. sourcecode:: python
    
    >>> 'walrus.'.capitalize()
    'Walrus'
    >>> 'WalRUs'.lower()
    'walrus'
    >>> 'WalRUs'.upper()
    'WALRUS'
    >>> 'I am the walrus'.split()
    ['I', 'am', 'the', 'walrus']
    >>> 'I am a twit'.replace('a', 'the')
    'I them the twit'

There are a *lot* more things you can do with strings if you want to.  If
there's something you can't see how to do, ask your teacher.


Other things
------------

It's possible to convert just about any object into a string that represents
it. So, for instance, 1234 would become the string ``'1234'``. The function
that does this is called ``repr`` --- short for representation. So, whatever
``x`` is, ``repr(x)`` is a string describing it.
