..
    Copyright Gareth McCaughan and Jeffrey Elkner. All rights reserved.
    
    CONDITIONS:

    A "Transparent" form of a document means a machine-readable form,
    represented in a format whose specification is available to the general
    public, whose contents can be viewed and edited directly and
    straightforwardly with generic text editors or (for images composed of
    pixels) generic paint programs or (for drawings) some widely available
    drawing editor, and that is suitable for input to text formatters or for
    automatic translation to a variety of formats suitable for input to text
    formatters. A copy made in an otherwise Transparent file format whose
    markup has been designed to thwart or discourage subsequent modification
    by readers is not Transparent. A form that is not Transparent is
    called "Opaque". 

    Examples of Transparent formats include LaTeX source and plain text.
    Examples of Opaque formats include PDF and Postscript.  Paper copies of
    a document are considered to be Opaque.

    Redistribution and use of this document in Transparent and Opaque
    forms, with or without modification, are permitted provided that the
    following conditions are met: 

    - Redistributions of this document in Transparent form must retain
      the above copyright notice, this list of conditions and the following
      disclaimer. 

    - Redistributions of this document in Opaque form must reproduce the
      above copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided with
      the distribution, and reproduce the above copyright notice in the
      Opaque document itself.

    - Neither the name of Scripture Union, nor LiveWires nor the names of
      its contributors may be used to endorse or promote products derived
      from this document without specific prior written permission. 

    DISCLAIMER:

    THIS DOCUMENT IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS,
    CONTRIBUTORS OR SCRIPTURE UNION BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS DOCUMENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Introducing Python
==================

.. index:: prompt, Python

First steps
-----------

To begin with, you should have a window on your screen with Python somewhere in
its title, displaying a line that looks something like this:

.. sourcecode:: python
    
    >>>


That ``>>>`` is called a **prompt**, which means it's something the computer
displays to tell you it's ready for your instructions. You can type things into
that window, and the computer will obey them when it understands your commands.

Unfortunately, the computer doesn't understand English. If you type in this:

.. sourcecode:: python
    
    >>> Tell me the sum of twelve and thirteen.

It won't understand it at all. Instead, because the computer doesn't speak our
language, you have to talk to it in a special language that is designed to be
easy for the computer --- and hopefully you --- to understand.

In fact, there are lots of languages designed for computers and humans to
communicate with each other. The one you're going to learn is called
**Python**. One of the good things about Python is that it's pretty easy for
humans to understand too.

Here's how you ask the computer to tell you the sum of twelve and thirteen. Try
it yourself.  You don't need to type in the ``>>>``, but you do need to hit the
key marked ``Enter`` after typing the line.

.. sourcecode:: python

    >>> 12 + 13

Here are some more examples, with the computer's answers shown too.

.. sourcecode:: python
    
    >>> 1 + 2 + 3 + 4
    10
    >>> 1 + 2 * 3 - 4        # Use * for multiplication, not x.
    3                        # If you expected 5, think again!
    >>> 200 * 300
    60000
    >>> 12 / 4               # Use / for division.
    3.0

Try experimenting some more with using Python as a calculator. Maybe try having
it do some more complicated things --- experiment with more numbers and bigger
numbers. Does it slow down at all? Or hesitate? Or is it pretty fast?

You can use parentheses to group operations --- anything you use to manipulate
a number, like ``+`` and ``*`` --- in Python just as you do in mathematics:

.. sourcecode:: python
    
    >>> (1 + 2) * (3 + 4)
    21

Here, Python has calculated (1 + 2) and (3 + 4) --- getting 3 and 7, and then
multiplied the results together.

Don't be afraid to experiment. Whenever you learn something new that you can do
with Python, try making slight changes --- or bigger changes ---  and play
around until you're confident that you understand just what's going on.  Don't
limit yourself to what's actually printed on the sheets!

Incidentally, if you're still confused about the fact that ``1 + 2 * 3 - 4``
gives ``3`` and not ``5``, the reason is that *multiplication happens before
addition*. Your math teacher probably calls this BODMAS, PEMDAS, or something
similar. If you're still confused, try asking your math teacher to explain it
to you.


.. index:: string, concatenate, list 

Different types of objects
--------------------------

So far, all the things you've worked with have been numbers. But Python can
handle plenty of things besides numbers. For instance, try the following:

.. sourcecode:: python
    
    >>> 'hello, ' + 'world'

What did you think the computer would do? What did it do?

Things between quotation marks are called **strings**. As you might guess from
the lines above, you can apply operations like ``+`` to strings as well as to
numbers. The plus sign **concatenates** strings; that is, puts one immediately
after the other. A little more surprising:

.. sourcecode:: python
    
    >>> 3 * 'hello'

You'll notice that this time we haven't told you what the machine says. That's
because you're supposed to try it for yourself. You won't learn anything if you
don't try the examples. Write down in your notes what the machine said when you
asked it for ``3 * 'hello'``, so that you remember.

You can surround strings in either single quotes or double quotes; Python
doesn't mind.

.. sourcecode:: python
    
    >>> 'ham' + "mock"
    'hammock'

Why would you care about that? Well, suppose you wanted a string containing the
text ``I'm sorry``? Try it.

Python also has **lists**:

.. sourcecode:: python
    
    >>> [1, 2, 3]
    [1, 2, 3]
    >>> [1, 2, 3] + [7, 8]

Again, we haven't told you what Python says to that last thing. Write it down
in your notes.


.. index:: name, variable

Giving names to things
----------------------

Suppose you know that you're going to need to do a lot of calculations
involving the number 123456 --- maybe it's your annual salary, or something.
You could just type the number in every time:

.. sourcecode:: python
    
    >>> 123456 * 3
    370368
    >>> 123456 / 6
    20576.0
    >>> 123456 - 1000
    122456

This might get very boring after a while. And if anyone else wanted to read
what you were doing, they might be confused by the mysterious number ``123456``
and wonder why it appeared so often.

You can solve either of these problems by giving the number a **name**. To save
typing, give it a short name, like ``n`` --- short for number, maybe. To make
it more obvious what it means, give it a longer name, like ``salary``. Here's
how you do that.

.. sourcecode:: python
    
    >>> salary = 123456
    >>> salary * 4
    493824
    >>> salary / 12
    10288.0
    >>> salary
    123456

The idea is that, after you've said ``salary = 123456``, you can always type
``salary`` instead of ``123456``. This is because the symbol ``=`` means is
assigned to , not is equal to like you were taught in math class. So now
``123456`` is assigned to the word ``salary``.

What we've called *names*, most people call **variables**. You'll find out
later why they're called that. For now, names is fine.

You can name things other than numbers, too. For instance:

.. sourcecode:: python
    
    >>> my_name = 'Gareth'
    >>> 'Hello, ' + my_name + '!'
    'Hello, Gareth!'


.. index:: expression 

Doing something over and over again
-----------------------------------

So far, you've done very little that your pocket calculator couldn't do equally
well. Here's something your calculator probably isn't so good at. The extra
spaces on the second line are important, by the way! This is explained in more
detail in `Sheet 2 <2-tables.html>`__ (*Turning the Tables*).

.. sourcecode:: python
    
    >>> for x in 1, 2, 3, 4, 5:
    ...     print(x, x * x)  # The prompt changes - Python is expecting more.
    ...                      # Just press Enter.

Can you guess what this will do?

... If you guessed that it prints out the numbers from 1 to 5 along with their
squares, well done. Notice that Python conveniently puts a space between the
two things you've asked it to print.

The ``print`` command is used when you want to make the computer display
things. The reason you haven't needed it before is that when you type it
something that has an answer Python can work out, it automatically displays
that answer. Things with answers are called **expressions**, for some reason.
But Python doesn't print out *every* value it computes; only the values of
expressions you type in at the ``>>>`` prompt, and things you tell it to print
out using the ``print`` command.


It's all gone horribly wrong
----------------------------

At some point when you're using Python, something like *this* is going to
happen.

.. sourcecode:: python
    
    >>> 3 + 'aardvark'
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: unsupported operand type(s) for +: 'int' and 'str'
    >>>

This looks pretty scary. Don't be scared. It's just Python's way of saying you
did something it didn't understand. You can probably ignore everything except
the last line. The last line is Python's way of telling us that it doesn't know
how to add a number to a string.

If you would like to dig deeper Python's into error messages, have a look at
`Sheet E <E-errors.html>`__ (*Errors*), which has a list of common complaints
Python might make at you and explanations of what they might mean.

If what we really want is to join them together using string concatenation, we
first have to convert the number to a string:

.. sourcecode:: python
    
    >>> str(3) + 'aardvark'
    '3aardvark'
    >>> str(7) + ' plus ' + str(3) + ' is ' + str(10) + '.'
    '7 plus 3 is 10.'
    >>>

Putting things inside ``str(...)`` converts them into strings.


.. index::
    single: Python shell
    single: program
    single: text file
    single: text editor

Writing programs in Python scripts
----------------------------------

So far you have only been using the **Python shell**, the thing that gives you
the prompt and waits for you instructions. We will keep using the shell
whenever we want to demonstrate new things Python can do.

Once we want to do a lot of things or to do the same thing over and over, the
shell is not so good. For one thing, it forgets what you typed after you quit
it. And if you make a mistake when typing something long, you have to start all
over again.

When we want to write **programs**, we will put them in a **text file** using a
**text editor**.

Your teacher will show you which editor to use and how to use it. The important
thing to remember is to give your file a name that ends with ``.py`` so that
your computer will know it is a Python script.

.. describe:: Challenge

    Write the following program in a file named ``firstprog.py``:

    .. sourcecode:: python
    
        print("Experiment 1: ")
        1 + 2
        print(1 + 2)
        "How come this did not print?"
        print("but this did?")
        print()
        print()
    
        print("Experiment 2:")
        for number in 2, 4, 6:
            print(2 * number)
    
        for number in 2, 4, 6:
            print(3 * number, end="")

    Run the program and look carefully at its output. What happens when you put
    an expression on a line without a ``print`` command in a Python script?
    Does it print? What does putting a comma at the end of a ``print`` command
    do? What does putting a ``print`` command on a line by itself do?

    Write down what you learned about printing. You will use this information
    often in the sheets ahead.


Graphics
--------

You can use Python for drawing pictures. Try this in a script. The first two
lines will probably seem rather weird; we'll explain them later.

.. sourcecode:: python
    
    from gasp import *

    begin_graphics()

    Line((100, 100), (200, 200))
    Circle((320, 240), 40)
    Box((400, 300), 100, 85)

    update_when('key_pressed')
    end_graphics()


What next?
----------

There are two kinds of worksheets for you to experiment with.

#. *Activity sheets*, each of which takes you through writing a program to do
   something that might be interesting. These sheets are numbered:
   `Sheet 2 <2-tables.html>`__ (*Turning the Tables*),
   `Sheet 3 <3-pretty.html>`__ (*Pretty Pictures*) and so on --- the sheet
   you've almost finished reading now is Sheet 1.
#. *Information sheets*, each of which tells you something useful
   about Python. These sheets are lettered: `Sheet D <D-dicts.html>`__
   (*Dictionaries*), `Sheet L <L-loops.html>`__ (*Loops*), or whatever.
   Usually the letters have something to do with what the sheets are about, but 
   that hasn't always been possible.

It's generally a good idea for you to work through the activity sheets in
order. Each one will point you to a few information sheets that you'll want to
have handy when working through it. The next one is
`Sheet 2 <2-tables.html>`__ (*Turning the Tables*).
