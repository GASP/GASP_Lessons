..
    Copyright Gareth McCaughan and Jeffrey Elkner. All rights reserved.
    
    CONDITIONS:

    A "Transparent" form of a document means a machine-readable form,
    represented in a format whose specification is available to the general
    public, whose contents can be viewed and edited directly and
    straightforwardly with generic text editors or (for images composed of
    pixels) generic paint programs or (for drawings) some widely available
    drawing editor, and that is suitable for input to text formatters or for
    automatic translation to a variety of formats suitable for input to text
    formatters. A copy made in an otherwise Transparent file format whose
    markup has been designed to thwart or discourage subsequent modification
    by readers is not Transparent. A form that is not Transparent is
    called "Opaque". 

    Examples of Transparent formats include LaTeX source and plain text.
    Examples of Opaque formats include PDF and Postscript.  Paper copies of
    a document are considered to be Opaque.

    Redistribution and use of this document in Transparent and Opaque
    forms, with or without modification, are permitted provided that the
    following conditions are met: 

    - Redistributions of this document in Transparent form must retain
      the above copyright notice, this list of conditions and the following
      disclaimer. 

    - Redistributions of this document in Opaque form must reproduce the
      above copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided with
      the distribution, and reproduce the above copyright notice in the
      Opaque document itself.

    - Neither the name of Scripture Union, nor LiveWires nor the names of
      its contributors may be used to endorse or promote products derived
      from this document without specific prior written permission. 

    DISCLAIMER:

    THIS DOCUMENT IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
    IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS,
    CONTRIBUTORS OR SCRIPTURE UNION BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS DOCUMENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Graphics API for Students of Python: GASP
=========================================


Introduction
------------

This sheet describes briefly all the graphics things (i.e., drawing pictures)
you can easily do with the gasp module for Python. (You can actually do lots of
other complicated graphics things with Python, but they're, errrm, complicated.
We've made a bunch of useful things easy using the gasp module, and these are
what this sheet describes.)

There's an introduction to graphics in `Sheet 3 <3-pretty.html>`__ (*Pretty
Pictures*), so this sheet is more a reference than an introduction. Some of it
is *very* terse; ask a teacher if it doesn't make any sense. (If you *are* a
teacher or you are studying this on your own and it still doesn't make any
sense, feel free to send an email to
`jeff.elkner@gmail.com <mailto:jeff.elkner@gmail.com>`__ .)


Coordinates
-----------

(0, 0) is at the bottom left of the window, with y increasing as you go upwards and x increasing as you go rightwards.
The window is 640 pixels by 480, by default. (You can make it a different size if you want to.)
Coordinates are given in units of one pixel.

All functions that take coordinates take them as a tuple (x, y).

.. sourcecode:: python
    
    Circle((300, 200), 10)     # :) This is good
    Circle(300, 200, 10)       # :( This is bad


Colors
------

To access the color module GASP has to offer. Call ``color.*`` where ``*`` is
the color you wish to call. For example: ``color.BLACK`` This is the color
black. You can also access colors as a string using the ``color.from_str(string)`` method.

*Note:* color names must be in ALL CAPS.

Check out the gasp color reference chart to see all of the available color
options, or call ``color.available()`` to see the list of available colors.
At their core, colors in GASP are HEX strings that specify which colors to draw on screen.


The Essentials
--------------

.. sourcecode:: python
    
    from gasp import *
    
    begin_graphics()
    
    ... 			# all of your code
    
    end_graphics()

These are the essentials. ``from gasp import *`` imports the gasp module,
``begin_graphics()`` starts the graphics window, and ``end_graphics()`` quits
the graphics window and ends the program. It's dead simple, but also dead
necessary.


Graphics Functions
------------------


begin_graphics()
~~~~~~~~~~~~~~~~

.. sourcecode:: python
    
    begin_graphics(width=800, height=600, background=color.YELLOW, title="My Game")

This creates a graphics window with the dimensions 800x600, a title of My Game
, and a background color of yellow. With no arguments you get a white 640x480
graphics window titled Gasp .

width
    The width of the window in pixels.

height
    The windows height in pixels.

background
    It is the background of the graphics window. It can either be a color or an
    image

title
    A string that will be the title of the window.


end_graphics()
~~~~~~~~~~~~~~

.. sourcecode:: python
    
    end_graphics() 

Ends a graphics window.


clear_screen()
~~~~~~~~~~~~~~

.. sourcecode:: python
    
    clear_screen()

Clears everything off of the graphics window. It looks like a new graphics
window as if you just called begin_graphics().


remove_from_screen()
~~~~~~~~~~~~~~~~~~~~

.. sourcecode:: python
    
    remove_from_screen(obj)

removes those objects from the screen

obj
    A screen object of a list of screen_objects you would like to remove from
    the screen


Screen Objects
--------------

The objects that you will be displayed in your graphics window. You can
manipulate these objects using the screen object methods


Plot
~~~~

.. sourcecode:: python
    
    Plot(pos, color=color.BLACK, size=1)

It puts a dot on the screen.

pos
    The coordinate on the screen that you wish to plot.

color
    The color you wish the dot to be.

size
    An integer that determines the size the of the dot


Line
~~~~

.. sourcecode:: python
    
    Line(start, end, color=color.BLACK)

Creates a line on the screen.

start
    The starting coordinate of the line.

end
    The coordinate at which the line will end.

color
    The color of the line

thickness
    The thickness in pixels of your line.


Box
~~~

.. sourcecode:: python
    
    Box(corner, width, height, filled=False, color=color.BLACK, thickness=1)

This creates a Box on the screen

corner
    A coordinate where the lower left corner of your box will be.

width
    The width in pixels of the box.

height
    The height of the box in pixels.

filled
    A boolean value that determines if your box will be filled

color
    The color of your box.

thickness
    The thickness in pixels of your box's lines.


Polygon
~~~~~~~

.. sourcecode:: python
    
    Polygon(points, filled=False, color=color.BLACK, thickness=1)


Creates a polygon on the screen

points
    A list of coordinates that is each point on the polygon. The must be more
    than two items in the list

filled
    A boolean value. If it is False the polygon will not be filled. Else, the
    polygon will not be filled

color
    The color of the polygon's lines

thickness
    An integer that determines the thickness of the lines.


Circle
~~~~~~

.. sourcecode:: python
    
    Circle(center, radius, filled=False, color=color.BLACK, thickness=1)

Draws a circle, its ``center`` is a set of coordinates, and the ``radius``
is in pixels. It defaults to not being filled and the color black.

center
    The circle's center coordinate.

width
    An integer that is the radius of the circle

filled
    A boolean value that determines if your circle will be filled

color
    The color of your circle.

thickness
    The thickness in pixels of the circles lines.


Arc
~~~

.. sourcecode:: python
    
    Arc(center, radius, start_angle, end_angle, filled=False, color=color.BLACK, thickness=1)

Creates an arc on the screen.

center
    A coordinate that is the center of the arc.

radius
    An integer that is the distance between the center and the outer edge of
    the arc.

start_angle
    The start angle in degrees of the arc

end_angle
    The end angle in degrees of your arc
    
filled
    A boolean value that if True it fills the arc

color
    The color the arc

thickness
    The thickness in pixels of the arc


Oval
~~~~

.. sourcecode:: python
    
    Oval(center, width, height, filled=False, color=color.BLACK, thickness=1)

Puts an oval on the screen wherever you want.

center
    The center coordinate of the Oval

width
    The width in pixels of the oval

height
    The height of the oval in pixels

filled
    A boolean value determining if the oval will be filled or not.
    
color
    The oval's color
    
thickness
    The thickness of the ovals lines


Screen Object Methods
---------------------

The methods that manipulates screen objects


move_to()
~~~~~~~~~

.. sourcecode:: python
    
    move_to(obj, pos)

Move a screen object to a pos

obj
    A screen object you wish to move.

pos
    The coordinate on the screen that the object will move to


move_by()
~~~~~~~~~

.. sourcecode:: python
    
    move_by(obj, dx, dy)

Move a screen object relative to it's position

obj
    The screen object you wish to move

dx
    How much the object will move in the 'x' direction. Positive or negative.

dy
    How much the object will move in the 'y' direction. A pixel value.


rotate_to()
~~~~~~~~~~~

.. sourcecode:: python
    
    rotate_to(obj, angle)

Rotate an object to an angle

obj
    The screen object that will be rotated

angle
    The angle in degrees that the object will be rotated to


rotate_by()
~~~~~~~~~~~

.. sourcecode:: python
    
    rotate_by(obj, angle)

Rotate an object a certain degree.

obj
    The screen object you wish to rotate
    
angle
    The degree that the object will be rotate. Can be positive or negative.


Text
----


Text()
~~~~~~

.. sourcecode:: python
    
    Text(text, pos, color=color.BLACK, size=12)

Puts text on the screen

text
    A string of the text that will be displayed
    
pos
    The center coordinate of the text

color
    The color of the text

size
    The font size


Keyboard
--------


keys_pressed()
~~~~~~~~~~~~~~

.. sourcecode:: python
    
    keys_pressed()

returns a list of all of the keys pressed at that moment.
