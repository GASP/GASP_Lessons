# GASP Python Course Lessons

A beginners Python course using the GASP module - a simple, immediate mode
graphics API.

## Build Instructions

On Debian 10:

1. Install make and sphinx:
```
$ sudo apt install make python3-sphinx
```

2. build the course from the sphinx source (assuming you are in the parent
   directory of the source directory)
```
$ make html
```

The HTML output will be located in the generated `build` directory.
