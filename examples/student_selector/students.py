from gasp import *


class World:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.circle_rad = 7*width/20
        self.Circle_center = (self.circle_rad, height-self.circle_rad)
        self.Circle = Circle((
            self.circle_rad, height-self.circle_rad),
            self.circle_rad,
            color=color.WHITE,
            filled=True
        )
        self.Box1 = Box((
            self.circle_rad-self.width/18, 0),
            self.width/9,
            height-self.circle_rad,
            color=color.WHITE,
            filled=True
        )
        self.Box2 = Box(
            (self.circle_rad-self.width/18, 0),
            self.width-(self.circle_rad-self.width/18),
            self.width/9,
            color=color.WHITE,
            filled=True
        )
        self.Box3 = Box(
            (8*self.width/9, 0),
            self.width/9,
            self.height,
            color=color.WHITE,
            filled=True
        )
        name_file = open("students.dat", 'r')
        self.names = name_file.readlines()
        self.students = []
        self.done_students = []
        self.selected = False
        for name in self.names:
            if name[0] == "#":
                break
            names = name.split("; ")
            self.students.append(Student(
                names[0],
                names[1][:-1],
                self.width/18,
                self.Circle_center,
                self.circle_rad,
                self
            ))

    def play(self):
        set_speed(200)
        self.running = True
        while self.running:
            for student in self.students:
                if student.done:
                    self.selected = False
                    self.students.remove(student)
                    self.done_students.append(student)
                    if len(self.students) == 0:
                        return
                student.update()
            if "enter" in keys_pressed() and not self.selected:
                self.students[random_between(0, len(self.students)-1)].select()
                self.selected = True
            update_when("next_tick")


class Student:
    colors = [
        color.RED, color.BLUE, color.YELLOW, color.GREEN,
        color.PINK, color.ORANGE
    ]

    def __init__(self, name, nickname, radius, globe_center,
                 globe_radius, world):
        self.world = world
        self.name = name
        self.nickname = nickname
        self.radius = radius
        self.globe_center = globe_center
        self.globe_radius = globe_radius
        direction = random_between(0, 360)
        distance = random_between(0, self.globe_radius-self.radius)
        self.x = distance * math.cos(math.radians(direction)) + \
            self.globe_center[0]
        self.y = distance * math.sin(math.radians(direction)) + \
            self.globe_center[1]
        self.Circle = Circle(
            (self.x, self.y),
            self.radius,
            color=Student.colors[random_between(0, len(Student.colors)-1)],
            filled=True
        )
        self.name_text = Text(self.name, (0, -30))
        length = get_text_length(self.name_text)
        move_to(self.name_text, (self.x-length/2, self.y))
        self.nick_text = Text(self.nickname, (0, -30))
        length = get_text_length(self.nick_text)
        move_to(self.nick_text, (self.x-length/2, self.y-12))
        self.direction = random_between(0, 360)
        self.speed = 2
        self.selected = False
        self.done = False
        self.bouncing = True
        self.state = "turn 1"

    def select(self):
        self.selected = True

    def move_by(self, dx, dy):
        self.x += dx
        self.y += dy
        self.Circle.move_by(dx, dy)
        self.name_text.move_by(dx, dy)
        self.nick_text.move_by(dx, dy)

    def update(self):
        if self.bouncing:
            dx = self.speed*math.cos(math.radians(self.direction))
            dy = self.speed*math.sin(math.radians(self.direction))
            self.move_by(dx, dy)
            self.bounce(dx, dy)
            if self.selected:
                if self.x > self.globe_center[0]-2 and \
                        self.x < self.globe_center[0]+2:
                    if self.y > (self.globe_center[1]-self.globe_radius)-2:
                        if self.y < (self.globe_center[1]-self.globe_radius)+2:
                            self.bouncing = False
                            dx = self.globe_center[0] - self.x
                            dy = (self.globe_center[1]-self.globe_radius) - \
                                self.y
                            self.move_by(dx, dy)
        else:
            if not self.check_touching():
                if self.state == "turn 1":
                    dy = -1*self.speed
                    dx = 0
                    self.move_by(dx, dy)
                    if self.y < self.radius:
                        self.state = "turn 2"
                        dy = self.radius - self.y
                        dx = 0
                        self.move_by(dx, dy)
                if self.state == "turn 2":
                    dx = 1*self.speed
                    dy = 0
                    self.move_by(dx, dy)
                    if self.x > self.world.width-self.radius:
                        self.state = "finish"
                        dx = (self.world.width-self.radius)-self.x
                        dy = 0
                        self.move_by(dx, dy)
                if self.state == "finish":
                    dx = 0
                    dy = 1*self.speed
                    self.move_by(dx, dy)
                    if self.y > self.world.height-self.radius:
                        dy = self.world.height-self.radius - self.y
                        dx = 0
                        self.move_by(dx, dy)
                        self.done = True
            else:
                self.done = True

    def check_touching(self):
        for student in self.world.done_students:
            if get_distance((self.x, self.y), (student.x, student.y)) < \
                    2*self.radius:
                return True
        return False

    def bounce(self, dx, dy):
        x = self.x + dx
        y = self.y + dy
        distance = get_distance((x, y), self.globe_center)
        # print distance, self.globe_radius-self.radius
        if distance > self.globe_radius-self.radius:
            if not self.selected:
                direction = 90 - math.degrees(math.atan2(
                    self.x-self.globe_center[0],
                    self.y-self.globe_center[1]
                ))
                direction += 180
                self.direction = random_between(
                    int(direction)-30,
                    int(direction)+30
                )
            else:
                self.direction = 90 - math.degrees(math.atan2(
                    self.globe_center[0]-self.x,
                    (self.globe_center[1]-self.globe_radius)-self.y
                ))


def get_distance(pos1, pos2):
    return math.sqrt((pos1[0]-pos2[0])**2+(pos1[1]-pos2[1])**2)


begin_graphics(900, 900, background=(100, 100, 100))
world = World(*screen_size())
world.play()
